class Planet {
	constructor(context, name, planet_color, orbit_radius, orbit_phase, planet_radius) {
		this.context = context;
		this.name = name;
		this.planet_color = planet_color;
		this.planet_radius = planet_radius;
		this.orbit_radius = orbit_radius;
		this.orbit_phase = orbit_phase;
		this.position = get_position_from_orbit(this.orbit_radius, this.orbit_phase);

		this.draw_planet();
		this.draw_orbit();
	}

	draw_planet() {
		// Set the Circle parameters
		this.context.beginPath();
		this.context.arc(this.position[0], this.position[1], this.planet_radius, 0, 2*Math.PI);
		// Draw the atmosphere
		this.context.strokeStyle = "#00B1FD";
		this.context.lineWidth = 0.25 * this.planet_radius;
		this.context.stroke();
		// Draw the actual planet
		this.context.fillStyle = this.planet_color;
		this.context.fill();
		// Planet name
		this.context.font = "11px Arial";
		this.context.fillStyle = "white";
		this.context.fillText(
			this.name, this.position[0] + this.planet_radius, this.position[1] + this.planet_radius
		);
	}

	draw_orbit() {
		// Set circle parameters
		this.context.beginPath();
		this.context.arc(centerX, centerY, this.orbit_radius, 0, 2*Math.PI);
		// Draw the orbit lines
		this.context.strokeStyle = this.planet_color;
		this.context.lineWidth = 1;
		this.context.stroke();
	}
}


class Faction {


}


class Fleet {
    constructor(context, faction, strength, origin, destination, travel_distance) {
        this.context = context;
        this.faction = faction;
        this.strength = strength;
        this.origin = origin;
        this.destination = destination;
        this.travel_distance = travel_distance;
        this.position = this.get_position(travel_distance);
        this.last_position = this.get_position(travel_distance - 0.2);
        this.draw_fleet();
    }

    get_position(travel_distance) {
        let position = interpolate(this.origin.position, this.destination.position, travel_distance);
        return position;

    }

    draw_fleet() {


        this.context.beginPath();
        this.context.rect(this.position[0] - 5, this.position[1] - 5, 10, 10);
		this.context.fillStyle = "#00B1FD";
		this.context.fill();

        var next_pos = this.get_position(this.travel_distance +0.08);

        this.context.rect(next_pos[0] - 3.7, next_pos[1] - 3.7, 7.5, 7.5);
		this.context.fillStyle = "#00B1FD";
		this.context.fill();

        this.context.beginPath();
        this.context.moveTo(this.origin.position[0], this.origin.position[1]);
        this.context.lineTo(this.destination.position[0], this.destination.position[1]);
        this.context.strokeStyle = "#00B1FD";
		this.context.lineWidth = 1.5;
        this.context.stroke();
    }
}

