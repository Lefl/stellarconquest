function get_position_from_orbit(radius, phase) {
	let position = []
	position[0] = centerX + radius * Math.cos(phase);
	position[1] = centerY + radius * Math.sin(phase);
	return position
}


function draw_background(context) {
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);
}


function draw_sun(context) {
	// Set the Circle parameters
	context.beginPath();
	context.arc(centerX, centerY, 25, 0, 2*Math.PI);
	// Draw the atmosphere
	context.fillStyle = "#FFFFBB";
	context.fill();
}


function interpolate(from, to, weight) {
    let delta = [];
    delta[0] = to[0] - from[0];
    delta[1] = to[1] - from[1];
    let position = [];
    position[0] = delta[0] * weight;
    position[1] = delta[1] * weight;
    position[0] += from[0];
    position[1] += from[1];

    return position;
}
